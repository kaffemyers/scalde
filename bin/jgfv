#!/bin/bash
dir=$(realpath "${1:-$PWD}")
cache_dir="${XDG_CACHE_HOME:-$HOME/.config/jgfv}"
[[ -f ${dir} ]] && dir=$(dirname "${dir}")
mkdir -p "${cache_dir}"

desktop_files=$(find \
	~/.local/share/applications/ \
	/usr/local/share/applications/ \
	/usr/share/applications/ \
	-iname '*.desktop')

mime_list="$HOME/.config/mimeapps.list"

_run_file() { 
	local temp
	temp=$(xdg-open "$1")
	if grep -q "no method available for opening" <<<"$temp"; then
		temp=$(xdg-mime query filetype "$1")
		if grep -q "^text" <<<"$temp"; then
			editor "$1"
		else
			notify-send "$1: do not know how to open file"
		fi
	fi
}

_get_icon() {
	local file=$1 mime_type dotdesktop
	mime_type=$(xdg-mime query filetype "$file")
	if grep -q "^image" <<<"$mime_type"; then
		ext=${file/*\.}
		sed -E 's/jpg/jpeg/; s/(bmp|gif|jpeg|png|tiff)/image-\1/; /image-/!s/.*/image-x-generic/' <<<"${ext,,}"
		return

	else

		if dotdesktop=$(grep -Fm1 "$mime_type" "$mime_list"); then
			dotdesktop="${dotdesktop#*=}"

		elif dotdesktop=$(grep -Fm1 "${mime_type/\/*}/*" "$mime_list"); then
			dotdesktop=${dotdesktop#*=}

		else
			dotdesktop=unconfigured
			echo utilities-terminal
		fi

		if [[ "$dotdesktop" != unconfigured ]]; then
			dotdesktop=$(grep -m1 -- "$dotdesktop$" <<<"$desktop_files")
			sed -n 's/^Icon=//p' "$dotdesktop"
		fi
	fi
}

_csv_output() {
	set -x
	local tmp icon file_or_dir=${1/ *} file_name=${1/[fd] } file_basename
	file_basename=$(basename "$file_name")

	file_safe_rex="$(sed 's/[^^]/[&]/g; s/\^/[\\\^]/g; s/\\/\\\\/g' <<<"$file_basename")"

	if tmp=$(grep "^$file_safe_rex$delimiter" "$cache_file"); then
		echo -n "${tmp//$delimiter/$'\n'}"
		return
	else
		sed -i "/^$file_safe_rex$delimiter/d" "${cache_file}"
	fi


	case "${file_or_dir}" in
		f) icon=$(_get_icon "${file_name}") ;;
		d) icon=folder ;;
		*) notify-send "Internal error getting icon"; exit;;
	esac
	
	echo "${file_basename}"
	echo "${file_or_dir} ${file_basename}"
	echo "${icon:-none}"
	echo "${file_basename}$delimiter${file_or_dir} ${file_basename}$delimiter${icon:-none}$delimiter" >>"${cache_file}"
}

_csv_factory() {
	local list delimiter='|___|' tmp_dir cache_file
	tmp_dir=$(realpath "$dir")
	cache_file="${cache_dir}/${tmp_dir//\//___}"
	touch "${cache_file}"

	if [[ "$tmp_dir" != "/" ]]; then
		echo "[Go up one folder]"
		echo "d .."
		echo "folder"
	fi
	
	while IFS= read -r; do
		list+=("$REPLY")
	done < <(find "${tmp_dir}" -maxdepth 1 -mindepth 1 -printf '%Y %p\n' | sort)
	export -f _csv_output _get_icon
	export mime_list desktop_files cache_file delimiter
	if [[ -n ${list[*]} ]]; then 
		parallel -k -j 0 _csv_output {} ::: "${list[@]}"
	fi
}

while [[ "$dir" =~ /\.\.(/|$) ]]; do printf -v dir '%s%s\n' "$(dirname "${dir/..*}")" "${dir#*..}"; done

while :; do

	[[ "$dir" =~ \.\.$ ]] && dir=$(dirname "$(dirname "$dir")")
	[[ "$dir" =~ ^// ]] && dir=${dir/\/}

	pick=$(_csv_factory | jgmenu_scalde --header "Exploring $dir" -f3 -c2 -n)
	p=${pick#* }
	case "${pick/ *}" in
		f  ) _run_file "${dir}/${p}"                                       ; break ;;
		d  ) dir="$dir/$p"                                                         ;;
		L  ) notify-send "${p} is a loop, so doing nothing"                ; break ;;
		N  ) notify-send "${p} leads to non-existing file, doing nothing"  ; break ;;
		\? ) notify-send "${p} is a link with unknown error, doing nothing"; break ;;		
		*  )                                                                 break ;;
	esac
done
