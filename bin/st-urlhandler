#!/bin/bash
# Using external pipe with st, give a jgmenu prompt of urls found in terminal,
# allowing the user to copy or follow in default browser.
# xclip required for this script.
# Original: https://github.com/LukeSmithxyz/st/blob/master/st-urlhandler
# Modified by Kaffe Myers

url_regex="(((http|https|gopher|gemini|ftp|ftps|git)://|www\\.)[a-zA-Z0-9.]*[:]?[a-zA-Z0-9./@$&%?$\#=_~-]*)|((magnet:\\?xt=urn:btih:)[a-zA-Z0-9]*)"

urls=$(sed 's/.*│//g' | tr -d '\n' |  # First remove linebreaks and mutt sidebars:
	grep -aEo "$url_regex" |            # grep only urls as defined above.
	uniq |                              # Ignore neighboring duplicates.
	sed "s/\(\.\|,\|;\|\!\\|\?\)$//;
	s/^www./http:\/\/www\./")           # xdg-open will not detect url without http

[[ -z "${urls}" ]] && exit 1

while getopts "hoc" o
	do case "${o}" in
		h) printf "Optional arguments for custom use:\\n  -c: copy\\n  -o: xdg-open\\n  -h: Show this message\\n" && exit 1 ;;
		o) sed -n "p;s/.*/xdg-open '&'/p" <<<"${urls}" | jgmenu_scalde --header 'Follow which url?' -f2;;
		c) echo "${urls}" | jgmenu_scalde --header 'Copy which url?' -f2 --no-spawn | tr -d '\n' | xclip -selection clipboard ;;
		*) printf "Invalid option: -%s\\n" "$OPTARG" && exit 1 ;;
	esac
done
