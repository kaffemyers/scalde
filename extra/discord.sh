#!/bin/bash
# Author:      Kaffe (the@kaffemyers.com)
# Created:     2021-06-27
# Description: Install latest discord. Quirky workaround for bullseye.

# shellcheck disable=SC1091 # irrelevant as shellcheck simply can't follow the logic -- it works fine
. "${SCALDE_REPOS_DIR:-/usr/local/src}/scalde/script_functions"
_check_super || exit 1

_echo_info green "Installing discord from static build download"

_install_packages gdebi curl

discord_url=$(curl -IsL "https://discord.com/api/download?platform=linux&format=deb" | sed '/location/!d;s/\S\+ //;s/\r//')

temp_dir=$(mktemp -d)
trap 'rm -rf "${temp_dir}"' EXIT
cd "${temp_dir}" || exit 1

curl -O "${discord_url}"
deb_file=$(basename "${discord_url}")

# fix for debian versions which doesn't have the required package and will thus mess up the install
if ! apt-cache search libappindicator 2>/dev/null | grep libapp | grep -v 'libayatana\|status-notif'; then
	dpkg-deb -R "${deb_file}" "${temp_dir}/discord-deb"
	sed -i 's/libappindicator1/libayatana-appindicator1/g' "${temp_dir}/discord-deb/DEBIAN/control"
	dpkg-deb -b "${temp_dir}/discord-deb" "${deb_file}"
fi

printf "y" | sudo gdebi "${deb_file}"

exit 0
