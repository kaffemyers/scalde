#!/bin/bash

# bitwarden and yad functions to simplify calls further down
_bw(){ bw "$@" --session "$(cat "$HOME/.bwsession")" ;}
_y(){ yad "${@:2}" --separator=$'\n' | sed '$d' >"$1" ;}

# generate a password that can handle most requirements
password=$(bw generate --passphrase --words 3 --separator $'\n' |
	sed "1s/./\U&/;  2s/\\S\\+$/${RANDOM:0:2}/" |
	shuf |
	paste -sd' ' |
	sed "s/$/$(printf '%s\n' "#" "$" "(" ")" "!" "<" ">" "_" "-" "+" "%" "=" "@" | shuf | sed '1!d')/")

# create the temporary files needed
td=$(mktemp -d /tmp/bwXXXXX)
trap 'rm -rf "$td"; ipcrm -M 111000 2>/dev/null' EXIT
_f(){ for ((i=0; i<"$1"; i++)); do mktemp -p "$td" yadputXXXXXX; done ;}
readarray -t res < <(_f 2)

# fetch folders
if ! folders_data=$(_bw list folders |
		jq -r '.[] | [.name, .id] | @csv' |
		sed '/"No Folder"/d'); then

	yad_args=(--center --form --title=Bitwarden "--text=Please enter Bitwarden credentials")
	if [[ "$folders_data" == "You are not logged in." ]]; then
		yad_args+=("--field=E-mail" "--field=Password:H")
		yad_output=$(yad "${yad_args[@]}" | sed 's/|$//')
		bw_args=(login "$(cut -d'|' -f1 <<<"$yad_output")" --passwordfile /dev/stdin)
		bw_master_password=$(cut -d'|' -f2- <<<"$yad_output")
	else
		yad_args+=("--field=Password:H)")
		bw_args=(unlock)
		bw_master_password=$(yad "${yad_args[@]}" | sed 's/|$//')
	fi
	echo "${bw_master_password}" | bw "${bw_args[@]}" | sed '$!d; s/.*--session\s*//' >"${HOME}/.bwsession"
	chmod 600 "${HOME}/.bwsession"

	folders_data=$(_bw list folders |
		jq -r '.[] | [.name, .id] | @csv' |
		sed '/"No Folder"/d')
fi

# fetch organizations
organizations_data=$(_bw list organizations |
	jq -r '.[] | [.name, .id] | @csv')

simple_url="$(sed 's,^.*://\(www\.\)\?\(.*\),\2,;  s,/.*,,;  s,:.*,,' <<<"${QUTE_URL}")"

folders_names=$( { echo '^No folder'; sed 's/",".*//;s/^"//' <<<"$folders_data" ;} | paste -sd '!')
organizations_names=$( { echo '^No organization'; sed 's/",".*//;s/^"//' <<<"$organizations_data" ;} | paste -sd '!')

standard_username=$(cat "$(realpath "$0".config)")

yad_fields=(
	--plug=111000 --tabnum=1 --form --columns=2
	--field="Username"        "$standard_username"        # 0
	--field="Entry name"      "$simple_url"             # 1
	--field="Uri"             "${QUTE_URL}"             # 2
	--field="Password":H      "$password"               # 3
	--field="Folder":CBE      "$folders_names"          # 4
	--field="Organization":CB "$organizations_names"    # 5
)
_y "${res[0]}" "${yad_fields[@]}" &

yad_fields=(
	--plug=111000 --tabnum=2 --form
	--field="Notes:TXT"                                 # 6
	--field="Favorite":CHK                              # 7
)
_y "${res[1]}" "${yad_fields[@]}" &

yad --paned --key=111000 --text="Add new Bitwarden entry" --tab="User" --tab="Note" --height=300 || exit

IFS=$'\n' readarray -t op < <(cat "${res[@]}")

folder_id=$(awk -F, -v on="\"${op[4]}\"" '$1 ~ on{ print $NF }' <<<"${folders_data}")
organization_id=$(awk -F, -v on="\"${op[5]}\"" '$1 ~ on{ print $NF }' <<<"${organizations_data}")
bw_input=("${organization_id:-null}" "${folder_id:-null}" "${op[1]}" "${op[6]}" "${op[7],,}" "${op[2]}" "${op[0]}" "${op[3]}")
schema='{
	"organizationId":%s,
	"folderId":%s,
	"type":1,
	"name":"%s",
	"notes":"%s",
	"favorite":%s,
	"login":{
		"uris":[{"match":null,"uri":"%s"}],
		"username":"%s",
		"password":"%s",
		"totp":null
	}
}'

# shellcheck disable=SC2059  # useless shellcheck, can't you see what's going on here?
if printf "$schema" "${bw_input[@]}" | bw encode | _bw create item; then
	_bw sync
else
	cat <<-EOF | yad --text-info --height=200 --width=200
		Creation of password unsuccessful.
EOF
fi
