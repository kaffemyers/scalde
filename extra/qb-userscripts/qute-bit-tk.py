#!/usr/bin/env python3
import json
import os
import re
import subprocess
import sys
import tkinter as tk
from time import sleep


# Simplify url for easier matching
URL = re.sub(r'.*://(www.)?', '', os.environ['QUTE_URL'])
URL = re.sub(r'/.*', '', URL)
URL = re.sub(r':.*', '', URL)

HOME = os.path.expanduser('~')
BW_SESS_FILE = os.path.join(HOME, '.bwsession')

# Set search method, different for ip and dns-name
if re.match(r'([0-9]+\.){3}[0-9]', URL):
    bw_method = 'search'
else:
    bw_method = 'url'


def user_exit():
    qute_command('message-info "Aborted by user"')
    sys.exit()


class Bw_gui():
    def __init__(self, func):
        self.root = tk.Tk(className='BitwardenTK')
        self.root.bind_class(
            'Button',
            '<Return>',
            lambda event: event.widget.invoke()
        )
        self.root.bind(
            '<Key-Escape>',
            lambda x: [
                self.root.destroy(),
                user_exit(),
            ]
        )
        self.root.bind('<Down>', lambda x: self.root.event_generate('<Tab>'))
        self.root.bind('<Up>', lambda x: self.root.event_generate('<Shift-Tab>'))
        self.button = []
        if func == 'get_pw':
            self.get_pw()
        elif func == 'pick_entry':
            self.pick_entry()

    def get_pw(self):
        self.label = tk.Label(self.root, text='Bitwarden master password')
        self.label.pack()
        self.root.bind(
            '<Return>',
            lambda *args: self.return_val(self.password.get()),
        )
        self.password = tk.StringVar()
        passEntry = tk.Entry(self.root, textvariable=self.password, show='*')
        passEntry.pack()
        passEntry.focus()
        self.button.append(
            tk.Button(
                self.root,
                text='Submit',
                command=lambda *args: self.return_val(self.password.get()),
            )
        )
        self.button[0].pack()
        self.root.mainloop()

    def make_entry_button(self):
        choice_val = bw_data.index(self.entry)
        butt_text = self.entry['name']
        self.button.append(
            tk.Button(
                self.root,
                text=butt_text,
                command=lambda *args: self.return_val(choice_val),
            )
        )
        self.button[-1].pack()

    def pick_entry(self):
        self.root.bind('<j>', lambda x: self.root.event_generate('<Tab>'))
        self.root.bind('<k>', lambda x: self.root.event_generate('<Shift-Tab>'))
        self.label = tk.Label(self.root, text='Pick Bitwarden password entry')
        self.label.pack()
        for bw_entry in bw_data:
            self.entry = bw_entry
            self.make_entry_button()
        self.button[0].focus_set()
        self.root.mainloop()

    def return_val(self, in_val):
        self.val = in_val
        self.root.destroy()

    def __str__(self):
        return str(self.val)


def bw_unlock():
    master_pw = str(Bw_gui('get_pw'))
    try:
        bw_out = subprocess.check_output(
            [
                'bw',
                'unlock',
                master_pw,
                '--raw',
                '--nointeraction',
            ]
        )
    except Exception:
        bw_out = b'Unsuccessful'

    with open(BW_SESS_FILE, 'w') as file:
        file.write(str(bw_out, 'utf-8'))


def bw_session():
    try:
        with open(BW_SESS_FILE, 'r') as file:
            token = file.read()
    except Exception:
        token = ''
    return token


def bw_cmd():
    try:
        bw_out = subprocess.check_output(
            [
                'bw',
                'list',
                'items',
                '--{0}'.format(bw_method),
                URL,
                '--nointeraction',
                '--session',
                bw_session(),
            ],
        )
    except Exception:
        return 'Unsuccessful'
    return bw_out


def js_print(user_or_pass, value):
    if user_or_pass == 'username':
        uop = 'if (isVisible(input) && (input.type == "text" || input.type == "email"))'
    elif user_or_pass == 'password':
        uop = 'if (input.type == "password")'

    # escape potential double quote characters for the js code
    value = re.sub('"', '\\"', value)

    js_code = """
        function isVisible(elem) {
            var style = elem.ownerDocument.defaultView.getComputedStyle(elem, null);
            if (style.getPropertyValue("visibility") !== "visible" ||
                style.getPropertyValue("display") === "none" ||
                style.getPropertyValue("opacity") === "0") {
                return false;
            }
            return elem.offsetWidth > 0 && elem.offsetHeight > 0;
        };
        function hasPasswordField(form) {
            var inputs = form.getElementsByTagName("input");
            for (var j = 0; j < inputs.length; j++) {
                var input = inputs[j];
                if (input.type == "password") {
                    return true;
                }
            }
            return false;
        };
        function loadData2Form (form) {
            var inputs = form.getElementsByTagName("input");
            for (var j = 0; j < inputs.length; j++) {
                var input = inputs[j];
                  {0} {
                    input.focus();
                    input.value = "{1}";
                    input.selecti();
                    input.dispatchEvent(new Event('change'));
                    input.blur();
                }
            }
        };
        var forms = document.getElementsByTagName("form");
        for (i = 0; i < forms.length; i++) {
            if (hasPasswordField(forms[i])) {
                loadData2Form(forms[i]);
            }
        }
    """
    # make above code interpreted correctly for .format
    js_code = re.sub(r'({|})', '\\1\\1', js_code)
    js_code = re.sub(r'{([01])}', '\\1', js_code)
    js_code = js_code.format(uop, value)
    # make it parseable by qutebrowser on a single line
    return js_code.replace('\n', ' ')


def qute_command(command):
    with open(os.environ['QUTE_FIFO'], 'w') as fifo:
        fifo.write('{0}{1}'.format(command, '\n'))
        fifo.flush()


if __name__ == '__main__':
    bw_data = bw_cmd()
    if bw_data == 'Unsuccessful':
        bw_unlock()
        bw_data = bw_cmd()
        if bw_data == 'Unsuccessful':
            qute_command('message-info {0}'.format(bw_data))
            sys.exit(bw_data)

    bw_data = json.loads(bw_data)
    bw_len = len(bw_data)
    if bw_len == 0:
        qute_command('message-info "No entry found for {0}"'.format(URL))
        sys.exit()
    elif bw_len == 1:
        choice = 0
    else:
        # please help me fix this
        choice = str(Bw_gui('pick_entry'))
        choice = int(choice)

    for ins in ('username', 'password'):
        js = js_print(ins, bw_data[choice]['login'][ins])
        qute_command('jseval -q {0}'.format(js))
        # some pages don't work with js entered values, this is a work-around
        qute_command('insert-text .')
        qute_command('later 30 fake-key <Backspace>')
        sleep(0.1)
    qute_command('mode-enter insert')
