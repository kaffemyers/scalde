#!/bin/bash
# Author:      Kaffe (the@kaffemyers.com)
# Created:     2021-04-19
# Description: Install Signal Desktop via official apt source

# shellcheck disable=SC1091 # irrelevant as shellcheck simply can't follow the logic -- it works fine
. "${SCALDE_REPOS_DIR:-/usr/local/src}/scalde/script_functions"
_check_super || exit 1

_echo_info green "Installing drawing via apt repo"

if apt-cache policy drawing | grep -q drawing; then
	_install_packages drawing
else
	_echo_info yellow "Couldn't find drawing through apt" "Skipping"
fi
