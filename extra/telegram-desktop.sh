#!/bin/bash
# Author:      Kaffe (the@kaffemyers.com)
# Created:     2021-04-19
# Description: Install Telegram Desktop static build

# shellcheck disable=SC1091 # irrelevant as shellcheck simply can't follow the logic -- it works fine
. "${SCALDE_REPOS_DIR:-/usr/local/src}/scalde/script_functions"
_check_super || exit 1

_install_packages wget xz-utils

_echo_info green "Installing linux static build for Telegram Desktop"
mkdir -p /opt
tarfile=/tmp/tsetup.tar.xz
wget -O $tarfile https://telegram.org/dl/desktop/linux
tar -xf $tarfile -C /opt
rm -f $tarfile
ln -vs /opt/Telegram/Telegram /usr/local/bin/telegram-desktop
