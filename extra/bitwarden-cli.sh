#!/bin/bash

# shellcheck disable=SC1091 # irrelevant as shellcheck simply can't follow the logic -- it works fine
. "${SCALDE_REPOS_DIR:-/usr/local/src}/scalde/script_functions"
_check_super

set -eu -o pipefail

temp_dir=$(mktemp -d)
trap 'rm -rf ${temp_dir}' EXIT
zip_name=${temp_dir}/bw_${RANDOM}.zip

_install_packages wget unzip

_echo_info green "Installing linux static build for bitwarden-cli"
wget -O "${zip_name}" "https://vault.bitwarden.com/download/?app=cli&platform=linux"
cd /tmp
unzip "${zip_name}"
mv -v bw /usr/local/bin/bw && chmod +x "${_}"
