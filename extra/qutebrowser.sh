#!/bin/bash
# Author:      Kaffe (the@kaffemyers.com)
# Created:     2020-11-10
# Description: Install qutebrowser in python3 venv

# shellcheck disable=SC1091 # irrelevant as shellcheck simply can't follow the logic -- it works fine
. "${SCALDE_REPOS_DIR:-/usr/local/src}/scalde/script_functions"
_check_super || exit 1

echo "Installing prerequisites via apt"
apt-get update
_install_packages --no-install-recommends git ca-certificates python3 python3-venv asciidoc libglib2.0-0 libgl1 libfontconfig1 libxcb-icccm4 libxcb-image0 libxcb-keysyms1 libxcb-randr0 libxcb-render-util0 libxcb-shape0 libxcb-xfixes0 libxcb-xinerama0 libxcb-xkb1 libxkbcommon-x11-0 libdbus-1-3 libyaml-dev gcc python3-dev libnspr4 libnss3
_install_packages gstreamer1.0-plugins-{bad,base,good,ugly} python3 python3-pip

_check_repo https://github.com/qutebrowser/qutebrowser.git
venv=/opt/qutebrowser-venv
python3 scripts/mkvenv.py --venv-dir ${venv}

# create wrapperscript and .desktop file and add to update-alternatives
echo -e "#!/bin/bash\n${venv}/bin/python3 -m qutebrowser \"\$@\"" > /usr/local/bin/qutebrowser
chmod 755 /usr/local/bin/qutebrowser
sed '/^Icon/s!qutebrowser!/usr/share/icons/qutebrowser/qutebrowser-128x128.png!' misc/org.qutebrowser.qutebrowser.desktop > /usr/share/applications/qutebrowser.desktop

# shellcheck disable=SC2154 # irrelevant because variable is sourced
ln -s "${SCALDE_REPOS_DIR:-/usr/local/src}/qutebrowser/icons" /usr/share/icons/qutebrowser 2>/dev/null
ln -s /etc/alternatives/x-www-browser /usr/bin/x-www-browser 2>/dev/null
update-alternatives --display x-www-browser 2>/dev/null | grep -q "/usr/local/bin/qutebrowser" || update-alternatives --install /usr/bin/x-www-browser x-www-browser /usr/local/bin/qutebrowser 91

normal_user=$(env | sed -n 's/SUDO_USER=//p')

if [[ ${normal_user:-root} != root ]] \
	&& ! sudo -u "${normal_user}" xdg-settings get default-web-browser | grep -q qutebrowser
then

	if _ask_user "Set qutebrowser as default for ${normal_user}?"; then
		sudo -u "${normal_user}" xdg-settings set default-web-browser qutebrowser.desktop
	fi
fi
