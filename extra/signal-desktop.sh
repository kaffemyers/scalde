#!/bin/bash
# Author:      Kaffe (the@kaffemyers.com)
# Created:     2021-04-19
# Description: Install Signal Desktop via official apt source

# shellcheck disable=SC1091 # irrelevant as shellcheck simply can't follow the logic -- it works fine
. "${SCALDE_REPOS_DIR:-/usr/local/src}/scalde/script_functions"
_check_super || exit 1

_install_packages wget gpg

sources=(
	'deb'
	'[arch=amd64'
	'signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg]'
	'https://updates.signal.org/desktop/apt'
	'xenial'
	'main'
)

sources_match="^\s*$(_join '\s+' "${sources[@]}")"
sources_list=/etc/apt/sources.list.d/signal-xenial.list

_echo_info green "Installing Signal Desktop by official signal apt repo"
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
mv signal-desktop-keyring.gpg /usr/share/keyrings/
! grep -qE "${sources_match//\[/\\[}" $sources_list && _join ' ' "${sources[@]}" | tee -a $sources_list
apt update
_install_packages signal-desktop
