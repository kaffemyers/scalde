#!/bin/bash

[[ "$(id -u)" != "0" ]] && echo "Must be run as root, directly or with sudo" && exit 1

temp_file=$(mktemp -p /tmp scalde.XXXXXX.tmp)
trap 'rm -r "$temp_file"' EXIT
if ! [[ -f "${SCALDE_REPOS_DIR:-/usr/local/src}/scalde/script_functions" ]]; then
	wget --no-check-certificate -qO "$temp_file" https://gitlab.com/kaffemyers/scalde/-/raw/master/script_functions || exit 1
	# shellcheck disable=SC1090
	source "$temp_file" || exit 1
else
	# shellcheck disable=SC1091
	source "${SCALDE_REPOS_DIR:-/usr/local/src}/scalde/script_functions" || exit 1
fi

_get_programs() {
	local csv="${scalde_dir}/programs.csv"

	if [[ $1 == commands ]]; then
		eval "$(awk -F, -v name="$2" '$2==name{print $3}' "$csv")"
	else
		awk -F, -v tag="$1" '$1==tag{print $2}' "$csv"
	fi
}


_echo_info --clear green <<-EOF
	Welcome to the scalde auto installer!

	This will take you through all you need to get
	scalde up and running. But what is scalde?
	A Linux desktop environment, the one I use.
	Regards
	Kaffe Myers
EOF
_ask_user "Continue?" || exit 1


if ! grep -q Debian < <(uname -a); then
	_echo_info red 'Scalde is only tested on Debian.' 'You probably should not continue.'
	_ask_user 'Continue anyway?' || exit
fi


_echo_info green "Making sure system is up to date"
apt-get update
apt-get -y upgrade
apt-get -y dist-upgrade

echo # newline
if _ask_user "Run autoremove, clean and purge old packages while we're at it?"; then
	apt-get -y autoremove
	apt-get clean

	eval "apt-get purge -y $(dpkg -l | awk '/^rc/{print $2}')"
fi


_echo_info --clear green "Setup the user account for the main user."
password=1 password_match=2 # make sure they do not match for initial check and password set logic
read -rp "Choose a name: " name
until grep -Eq "^[a-z_][a-z0-9_-]*$" <<<"$name"; do read -rp "Invalid username. Must start with a letter, all lowercase: " name; done

if id -u "$name" &>/dev/null; then
	_echo_info --clear yellow <<-EOF
		BEWARE!

		A user named $name already exist.
		How do you want to deal with this?
EOF

	user_home=$(getent passwd "$name" | cut -d: -f6)
	backup_name=-backup-$(date +%F-%T | sed 's/[:-]//g')

	cat <<-EOF
		    1. Append "-backup" to home folder and create a new one to avoid conflict
		    2. Overwrite any config files in current home folder
		    3. Exit installer
EOF
	while :; do
		read -rp "Choice: " choice
		echo
		case "$choice" in
			1) mv "$user_home" "$user_home$backup_name"; break;;
			2) break;;
			3) exit 2;;
			*) echo "invalid input, only enter 1, 2 or 3.";;
		esac
	done
else

	until [[ "$password" == "$password_match" ]]; do
		[[ $check == failed ]] && echo -e "\nPassword mismatch, please try again."
		read -rsp "Enter password: " password
		read -rsp $'\n'"Re-enter password: " password_match
		echo
		check=failed
	done
fi


user_home=${user_home:-/home/$name}
groupadd "$name" 2>/dev/null
useradd -m -g "$name" -s /bin/bash "$name" &>/dev/null ||
usermod -a -G "$name" "$name" && mkdir -p "$user_home" && chown "$name:$name" "$user_home"
usermod -a -G "sudo" "$name"
[[ "$password" == "$password_match" ]] && passwd kaffe <<<"$password"$'\n'"$password_match"
unset password password_match


debian_release=$(lsb_release -sc)
if [[ ${debian_release} != bookworm ]]; then
	_echo_info --clear yellow <<-EOF
		You are running ${debian_release}.
		Scalde is developed for Debian testing (bookworm at the time of writing).
		Minor testing suggests it works well on Debian bullseye as well.
EOF

	_ask_user "Continue installation on ${debian_release}?" || exit
fi


_echo_info --clear green "Fetching scalde repo and installing" "needed apt and python packages"
_install_packages --no-install-recommends git

_check_repo https://gitlab.com/kaffemyers/scalde.git
scalde_dir="${SCALDE_REPOS_DIR}/scalde"
! [[ -d "${scalde_dir}" ]] && _echo_info red "Couldn't enter scalde repo," "something must have gone wrong." && exit 1

_install_packages $(_get_programs apt)
_install_packages --no-install-recommends $(_get_programs no-recommends)
python3 -m pip install -U $(_get_programs pip)

# loop through git repositories to add
clear
while IFS= read -r; do
	_echo_info green "Installing $(basename "${REPLY%.git}" "Source: $REPLY")"
	_check_repo "$REPLY"
	_get_programs commands "$REPLY"
done < <(_get_programs git)


# set st as standard terminal
if ! update-alternatives --display x-terminal-emulator 2>/dev/null | grep -q "/usr/local/bin/st"; then
	update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator "/usr/local/bin/st" 50
fi
cat <<-EOF >/usr/share/applications/st.desktop
	[Desktop Entry]
	Type=Application
	Name=st
	Comment=simple-terminal emulator for X
	Icon=utilities-terminal
	Exec=st
	Categories=System;TerminalEmulator
EOF


###{ capitaine-cursor theme install
_echo_info green "Installing subjectively nicer icon theme"
cd "${scalde_dir}" || exit 1
tar xvf capitaine-cursors-r4.tar.gz
cp -pr capitaine-cursors/ /usr/share/icons/capitaine-cursors
rm -rf capitaine-cursors
update-alternatives --install /usr/share/icons/default/index.theme x-cursor-theme /usr/share/icons/capitaine-cursors/index.theme 91
###} END capitaine-cursor theme install


###{ Ultimate Dark (flat) Orange gtk theme install
_echo_info green "Installing Ultimate Dark gtk theme"
cd "${scalde_dir}" || exit 1
tar xvf 'Ultimate-Dark-(Flat)-Orange.tar.xz'
cp -pr 'Ultimate-Dark-(Flat)-Orange/' '/usr/share/themes/Ultimate-Dark-(Flat)-Orange'
rm -rf 'Ultimate-Dark-(Flat)-Orange'
###{ END Ultimate Dark (flat) Orange gtk theme install


###{ scalde wrapper scripts
_echo_info green "Installing scalde helper scripts" "Location: /usr/local/bin"
for scalde_script in "${SCALDE_REPOS_DIR}"/scalde/bin/*; do
	cp -v "${scalde_script}" "/usr/local/bin/$(basename "${scalde_script}")"
done
###} END scalde wrappter scripts


###{ sudoers edits
_echo_info green "Add passwordless system shutdown for sudoers"
sudo_passwordless=(
	"/usr/sbin/halt"
	"/usr/sbin/reboot"
	"/usr/sbin/poweroff"
	"/usr/bin/systemctl suspend"
	"/usr/bin/systemctl hibernate"
	"/usr/local/bin/scalde_wifi_menu"
)

(IFS=, ; sed 's/,/, /g;s/^/%sudo\tALL=NOPASSWD: /' <<<"${sudo_passwordless[*]}") >/etc/sudoers.d/scalde-control
###} END sudoers edits


###{ config files
cd "${SCALDE_REPOS_DIR}/scalde" || exit 1
_echo_info green "Setting up scalde dotfiles"
bash "./create-standard-config.sh" "$name"
###} END config files


###{ extra apps
cd "${SCALDE_REPOS_DIR}/scalde" || exit 1
_echo_info green "Additional/optional application installation"
_ask_user "Do you want to install extra app(s)?" && bash "./extra-install.sh" || echo Skipping.
###} END extra apps

exit 0
