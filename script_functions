#!/bin/bash

# if not set, use default location
SCALDE_REPOS_DIR=${SCALDE_REPOS_DIR:-/usr/local/src}
mkdir -p "${SCALDE_REPOS_DIR}"


_install_packages() {
	local p a=("--yes")
	for package in "$@"; do
		if [[ "$package" =~ ^- ]]; then
			a+=("${package}")
		elif ! dpkg-query -l "${package}" | grep -q '^ii '; then
			p+=("${package}")
		fi
	done
	if [[ "${p[0]:-none}" != none ]]; then
		echo "Installing prerequisitase: ${p[*]}"
		apt-get install "${a[@]}" "${p[@]}" || return 1
	fi
	return 0
}


# nice, colored, bold information text box
_echo_info() {

	while :; do case "$1" in
		--clear) clear;                 shift ;;
		red    ) echo -e '\033[00;31m'; break ;;
		green  ) echo -e '\033[00;32m'; break ;;
		yellow ) echo -e '\033[00;33m'; break ;;
		*      ) echo "Script bug, maintainer made a boo boo." && exit 1
	esac; done
	shift

	[[ -z "$*" ]] && unset TEMP && while IFS= read -r; do TEMP+=("$REPLY"); done && set -- "${TEMP[@]}"
	# character map:
	# ul = upper left, ur = upper righ
	# ll = lower left, lr = lower right
	# hl = horizontal line, vl = vertical line

	printf '%s\n' "${@}" | awk -v i=1 -v padding=1 \
		-v ul="╔" -v ur="╗" \
		-v ll="╚" -v lr="╝" \
		-v hl="═" -v vl="║" \
		'BEGIN{ p=sprintf( "%" padding "s", " " ) }

			{
				line[i]=p $0 p
				l=length(line[i])
				if(l > len) { len=l }
				i++
			}

			END{
				horizontal=sprintf("%0" len "d", 0)
				gsub("0", hl, horizontal)

				print ul horizontal ur
				for(i in line) {
					printf vl "%-" len "s" vl "\n", line[i]
				}
				printf ll horizontal lr
			}'

	echo -e "\033[0m\n"
}


# check if super user/sudo
_check_super() {
	if [[ "$(id -u)" == "0" ]]; then
		return 0
	else
		echo "Must be run as root, directly or with sudo"
		return 1
	fi
}


# print message and prompt for yes or no
_ask_user(){
	echo -n "$1"
	shift
	echo -en "${*/#/\\n}"
	local answer
	while : ; do
		read -r -p " (y/n) " answer
		case ${answer,,} in
			y|yes ) return 0 ;;
			n|no  ) return 1 ;;
			*     ) echo "Enter y or n" ;;
		esac
	done
}


# clone or update git repo, and cd into it
_check_repo() {
	cd "${SCALDE_REPOS_DIR}" || return 1
	local repourl=${1} reponame
	reponame=$(basename "${repourl%.git}")
	if [[ ! -d ${reponame} ]] ; then
		echo "Cloning into ${PWD}/${reponame}"
		git clone -q "${repourl}"
		cd "${reponame}" || return 1
	else
		echo "Looking for updates for ${PWD}/${reponame}"
		cd "${reponame}" || return 1
		git pull -q
	fi
}


# join strings by any delimiter (multi-character as well)
_join() {
	delimiter="$1"
	first="$2"
	shift 2 || return 1
	printf %s "${first}" "${@/#/${delimiter}}"
}
