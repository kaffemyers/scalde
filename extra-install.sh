#!/bin/bash
# Author:      Kaffe (the@kaffemyers.com)
# Created:     2020-11-10
# Description: Install some extra applications to make it a more complete
#              desktop environment. Right now only qutebrowser, will probably
#              add more apps later.

# shellcheck disable=SC1091 # irrelevant as shellcheck simply can't follow the logic -- it works fine
. "${SCALDE_REPOS_DIR:-/usr/local/src/scalde}/script_functions"

# only run as root/with sudo
_check_super || exit

unset install_scripts
while IFS= read -r; do install_scripts+=("$REPLY"); done < <(find extra -maxdepth 1 -type f)
for extra in "${install_scripts[@]}"; do
	_ask_user "Install $(basename "${extra%.sh}")?" && bash "${extra}" && echo -e "...done!\n"
done
