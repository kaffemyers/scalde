# scalde

**Somewhat Convoluted Albeit Lightweight Desktop Environment**

A very custom desktop environment under development whenever I have some time and ideas. Developed for Debian testing mainly, should probably work with bullseye as well.

**_BEWARE:_**

Right now I'm making small changes daily and unstrucurally. When and if I feel it is actually ready for the public, this README will be clear on that. I.e., the main branch IS the development branch. I will make a development branch when there is a stable version to speak of.
Try it out at your own risk.

## Notable installs

All installations which isn't standard apt uses custom installation scripts to get all the hassle out of the way and straight to business.
Check the install script if you wish to know exactly what is installed, but here's a list of packages that are worth to mention to get an idea of the experience scalde provides:

### Main desktop environment

| Type                  | Name                      | Source                                                       | Additional info        |
| --------------------- | ------------------------- | ------------------------------------------------------------ | ---------------------- |
| Display manager       | ly                        | [git](https://github.com/nullgemm/ly)                        |                        |
| Window manager        | bspwm                     | [git](https://github.com/baskerville/bspwm)                  |                        |
| Hotkey manager        | sxhkd                     | [git](https://github.com/baskerville/sxhkd)                  |                        |
| Terminal emulator     | st                        | [git](https://github.com/LukeSmithxyz/st)                    | jgmenu script alternatives included |
| Destop panel          | tint2                     | apt ([project link](https://gitlab.com/o9000/tint2))         |                        |
| Quick lanch and menus | jgmenu                    | apt ([project link](https://github.com/johanmalm/jgmenu))    |                        |
| Desktop notifications | dunst                     | apt ([project link](https://github.com/dunst-project/dunst)) |                        |
| File manager          | ranger                    | [git](https://github.com/hut/ranger)                         |                        |
| GTK theme             | Ultimate Dark Flat Orange | [git](https://github.com/bolimage/Ultimate-Maia)             | provided as an archive |
| Icon theme            | crule                     | [git](https://github.com/cutefishos/icons)                   |                        |
| Mouse cursor          | capitain-cursors          | [git](https://github.com/keeferrourke/capitaine-cursors)     | provided as an archive |

### Prompts if you wish to install

| Type               | Name             | Source                                                     | Additional info               |
| ------------------ | ---------------- | ---------------------------------------------------------- | ----------------------------- |
| Web browser        | qutebrowser      | [git](https://github.com/qutebrowser/qutebrowser)          | python3-venv                  |
| Password manager   | bitwarden-cli    | [static build](https://bitwarden.com)                      |                               |
| Communication tool | signal-desktop   | [apt](https://signal.org/download/)                        | signal provided repo          |
| Communication tool | telegram-dekstop | [static build](https://telegram.org/)                      |                               |
| Communication tool | discord          | [static build](https://discord.com/)                       |                               |
| Painting tool      | drawing          | apt ([project link](https://maoschanz.github.io/drawing/)) | mainly for screenshot editing |

### Bonus

- A few experimental bitwarden implementations for qutebrowser in extra/qb-userscripts

To install scalde, run the following command (as root)

    bash -c "$(wget --no-check-certificate -qO - https://gitlab.com/kaffemyers/scalde/-/raw/master/install.sh)"

## Hotkeys

[Check this md.](https://gitlab.com/kaffemyers/scalde/-/blob/master/hotkeys.md)

## Themes

For a quick change of the look and feel, just configure tint2 to your liking and the rest should follow suite. This can either be done with tint2 settings panel configuration gui, or via the config file `~/.config/tint2/tint2rc`. 

In theory, just slapping on a new tint2 and a custom background image (set background path in `~/.config/scalde/settings`) should be enough to get it more sparkly. Of course, you can still go in and change every aspect as the linking is merely some calls in the bspwmrc and a config switch in jgmenu respectively. To change everything, there's also .Xdefault, the gtk theme and so on. You do you.

The original theme is not meant to blow anyone away in a design contest, it's more about taking away distractions with dark and toned down color schemes. This being said, if someone feels like prettying up the original theme while still keeping the original idea in mind, feel free to contribute to the project!


### OK, this all looks super cool and fantastic, but I like tabbed windows, and scalde doesn't have it.

Sure, just go to [bsptab](https://github.com/albertored11/bsptab) and you can sort that out!

## To-do

Besides a great many things... here's a couple of clear ones:

- [ ] Switch to xssstate and slock instead of xscreensaver
- [ ] yad for settings
- [ ] jgmenu tweaks for better look on custom menus
- [ ] fix lacking ranger preview (images for starters)

## Contributions

Besides all the amazing work done by the people that created all these wonderful packages that I've chosen to sew this together, it's currently just me, Kaffe, doing the commits. There are a lot of things I don't know how to do in the best way and would not mind other people butting in with their oppinions and eventual pull requests.
