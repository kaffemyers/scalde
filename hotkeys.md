# Scalde hotkeys

## scalde desktop

| Description                 | Hotkey                                                                                          |
| --------------------------- | ----------------------------------------------------------------------------------------------- |
| terminal emulator           | <kbd>super</kbd> + <kbd>Return</kbd>                                                            |
| file manager                | <kbd>super</kbd> + <kbd>e</kbd>                                                                 |
| launcher                    | <kbd>super</kbd> + <kbd>space</kbd>                                                             |
| run dialog                  | <kbd>super</kbd> + <kbd>r</kbd>                                                                 |
| lock session                | <kbd>super</kbd> + <kbd>Delete</kbd>                                                            |
| wifi create/pick connection | <kbd>super</kbd> + <kbd>shift</kbd> + <kbd>n</kbd>                                              |
| volume control              | Media keys: <kbd>RaiseVolume</kbd>, <kbd>LowerVolume</kbd>, <kbd>Mute</kbd>, <kbd>MicMute</kbd> |

## st hotkeys

Usable when terminal is in focus.

| Description           | Hotkey                        |
| --------------------- | ----------------------------- |
| copy selection        | <kbd>alt</kbd> + <kbd>c</kbd> |
| paste from clipboard  | <kbd>alt</kbd> + <kbd>v</kbd> |
| open link in browser  | <kbd>alt</kbd> + <kbd>l</kbd> |
| yank (copy) link      | <kbd>alt</kbd> + <kbd>y</kbd> |
| yank command output   | <kbd>alt</kbd> + <kbd>o</kbd> |
| scrollback (one line) | <kbd>alt</kbd> + <kbd>Up</kbd> / <kbd>Down</kbd> |
| scrollback (multiple lines) | <kbd>alt</kbd> + <kbd>Pageup</kbd> / <kbd>Pagedown</kbd> |
| scrollback (multiple lines) | <kbd>alt</kbd> + <kbd>u</kbd> / <kbd>d</kbd> |
| change font size (one step) | <kbd>shift</kbd> + <kbd>alt</kbd> + <kbd>Up</kbd> / <kbd>Down</kbd> |
| change font size (multiple steps) | <kbd>shift</kbd> + <kbd>alt</kbd> + <kbd>Pageup</kbd> / <kbd>Pagedown</kbd> |
| change font size (multiple steps) | <kbd>shift</kbd> + <kbd>alt</kbd> + <kbd>u</kbd> / <kbd>d</kbd> |
| change terminal background transparency | <kbd>shift</kbd> + <kbd>a</kbd> / <kbd>s</kbd> |

## Print screen

| Description             | Hotkey                                                                 |
| ----------------------- | ---------------------------------------------------------------------- |
| screen selection        | <kbd>Print</kbd>                                                       |
| whole desktop           | <kbd>super</kbd> + <kbd>Print</kbd>                                    |
| active window           | <kbd>super</kbd> + <kbd>ctrl</kbd> + <kbd>Print</kbd>                  |
| upload screen selection | <kbd>alt</kbd> + <kbd>Print</kbd>                                      |
| upload whole desktop    | <kbd>alt</kbd> + <kbd>super</kbd> + <kbd>Print</kbd>                   |
| upload active window    | <kbd>alt</kbd> + <kbd>super</kbd> + <kbd>ctrl</kbd> + <kbd>Print</kbd> |

## bspwm hotkeys

| Description                                                                                | Hotkey                                                             |
| ------------------------------------------------------------------------------------------ | ------------------------------------------------------------------ |
| system quit dialog                                                                         | <kbd>super</kbd> + <kbd>Escape</kbd>                               |
| close window                                                                               | <kbd>super</kbd> + <kbd>w</kbd>                                    |
| kill window                                                                                | <kbd>super</kbd> + <kbd>shift</kbd> + <kbd>w</kbd>                 |
| toggle tiled/monocle layout                                                                | <kbd>alt</kbd> + <kbd>space</kbd>                                  |
| if the current node is automatic, send it to the last manual, otherwise pull the last leaf | <kbd>super</kbd> + <kbd>y</kbd>                                    |
| if the current node is automatic, send it to the last manual, otherwise pull the last leaf | <kbd>super</kbd> + <kbd>button2</kbd>                              |
| swap current and biggest node                                                              | <kbd>super</kbd> + <kbd>g</kbd>                                    |

## state/flags

| Description                                 | Hotkey                                                                                       |
| ------------------------------------------- | -------------------------------------------------------------------------------------------- |
| set the window state (all)                  | <kbd>super</kbd> + <kbd>t</kbd>/ <kbd>s</kbd>/ <kbd>f</kbd>                                  |
| set the window state (tiled, pseudo tiled)  | <kbd>super</kbd> + <kbd>ctrl</kbd> + <kbd>button4</kbd>                                      |
| set the window state (floating, fullscreen) | <kbd>super</kbd> + <kbd>ctrl</kbd> + <kbd>button5</kbd>                                      |
| set the node flags                          | <kbd>super</kbd> + <kbd>ctrl</kbd> + <kbd>x</kbd>/ <kbd>y</kbd>/ <kbd>z</kbd>                |

## focus/swap

| Description                                        | Hotkey                                                                                                         |
| -------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- |
| focus the node in the given direction              | <kbd>super</kbd> + <kbd>h</kbd>/ <kbd>j</kbd>/ <kbd>k</kbd>/ <kbd>l</kbd> (hold <kbd>shift</kbd> to move node) |
| focus the node for the given path jump             | <kbd>super</kbd> + <kbd>p</kbd>/ <kbd>b</kbd>/ <kbd>comma</kbd>/ <kbd>period</kbd>                             |
| focus the next/previous node                       | <kbd>alt</kbd> + <kbd>Tab</kbd> (hold <kbd>shift</kbd> to reverse)                                             |
| focus the next/previous desktop                    | <kbd>super</kbd> + <kbd>bracketleft</kbd>/ <kbd>right</kbd>                                                    |
| focus the last node/desktop                        | <kbd>super</kbd> + <kbd>grave</kbd>/ <kbd>Tab</kbd>                                                            |
| focus the older or newer node in the focus history | <kbd>super</kbd> + <kbd>o</kbd>/ <kbd>i</kbd>                                                                  |
| focus or send to the given desktop                 | <kbd>super</kbd> + <kbd>1-9</kbd>/ <kbd>0</kbd> (hold <kbd>shift</kbd> to move node)                           |

## preselect

| Description                                     | Hotkey                                                                                            |
| ----------------------------------------------- | ------------------------------------------------------------------------------------------------- |
| preselect the direction                         | <kbd>super</kbd> + <kbd>ctrl</kbd> + <kbd>h</kbd>/ <kbd>j</kbd>/ <kbd>k</kbd>/ <kbd>l</kbd>       |
| preselect the direction                         | <kbd>super</kbd> + <kbd>button6</kbd>/ <kbd>button5</kbd>/ <kbd>button4</kbd>/ <kbd>button7</kbd> |
| preselect the ratio                             | <kbd>super</kbd> + <kbd>ctrl</kbd> + <kbd>1-9</kbd>                                               |
| cancel the preselection for the focused node    | <kbd>super</kbd> + <kbd>ctrl</kbd> + <kbd>space</kbd>                                             |
| cancel the preselection for the focused desktop | <kbd>super</kbd> + <kbd>ctrl</kbd> + <kbd>shift</kbd> + <kbd>space</kbd>                          |
| cancel the preselection for the focused desktop | <kbd>super</kbd> + <kbd>button9</kbd>                                                             |

## move/resize

| Description                                               | Hotkey                                                                                                        |
| --------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------- |
| expand a window by moving one of its side outward         | <kbd>super</kbd> + <kbd>alt</kbd> + <kbd>h</kbd>/ <kbd>j</kbd>/ <kbd>k</kbd>/ <kbd>l</kbd>                    |
| contract a window by moving one of its side inward        | <kbd>super</kbd> + <kbd>alt</kbd> + <kbd>shift</kbd> + <kbd>h</kbd>/ <kbd>j</kbd>/ <kbd>k</kbd>/ <kbd>l</kbd> |
| move a floating window                                    | <kbd>super</kbd> + <kbd>Left</kbd>/ <kbd>Down</kbd>/ <kbd>Up</kbd>/ <kbd>Right</kbd>                          |
| insert receptacle or clear receptacles on current desktop | <kbd>super</kbd> + <kbd>ctrl</kbd> + <kbd>r</kbd> (hold <kbd>shift</kbd> to reverse)                          |

