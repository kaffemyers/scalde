#!/bin/bash
# Author:      Kaffe (the@kaffemyers.com)
# Created:     2020-11-11
# Description: Add standard config files for scalde

script_functions_path="${SCALDE_REPOS_DIR:-/usr/local/src}/scalde/script_functions"
# shellcheck disable=SC1090 # irrelevant because shellcheck simply cant read above variable properly
. "${script_functions_path}"

# set value for root/sudo or user run
_check_super &>/dev/null && user_name=$(env | sed '/^SUDO_USER/!d;s/.*=//') || user_name=${USER}

if [[ -n "${1}" ]]; then
	user_name=${1}
	id -u "$user_name" &>/dev/null || { echo "${user_name} doesn't exist"; exit 1 ;}

elif [[ "${user_name}" != "${USER}" ]]; then
	if ! _ask_user "${user_name} identified as sudo user." "Install standard config files for ${user_name}?"; then
		echo "Which user do you wish to install standard config files for?"
		while : ; do
			read -rp "Username: " user_name
			if id -u "${user_name}" &>/dev/null; then
				break
			else
				echo "User doesn't exist, try again. (Ctrl+C to quit)"
			fi
		done
	fi
fi

cd "$(dirname "${script_functions_path}")/dotfiles" || exit 1
user_home=$(getent passwd "$user_name" | cut -d: -f6)
user_gidn=$(id -gn kaffe)

while read -r file; do
	destination_file="$user_home/$file"
	destination_dir=$(dirname "$destination_file")
	mkdir -p "$destination_dir"
	chown "$user_name":"$user_gidn" "$destination_dir"
	cp -v "$file" "$destination_file"
	chown "$user_name":"$user_gidn" "$destination_file"
done < <(find . -type f -printf '%P\n')

exit 0
